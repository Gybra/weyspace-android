package com.F00lishDevTeam.weyspace;

import java.util.ArrayList;

import org.holoeverywhere.widget.ProgressBar;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import android.content.Context;
import android.database.DataSetObserver;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ExpandableAdapaterCustom implements ExpandableListAdapter{

	int layout, layout_child,count_child;
	JSONArray json;
	Context c;
	JSONObject imagePage,jsonPage;
	ArrayList arrayUsersImage,arrayUsersName;
	TextView groupText,name;
	ProgressBar votes;
	ImageView image;
	String search;
	ImageLoaderConfiguration config;
	ImageLoader img;
	private DisplayImageOptions d;
	
	ExpandableAdapaterCustom(Context c, int layout, int layout_child, JSONObject imagePage, JSONObject jsonPage, ArrayList arrayUsersImage,
			ArrayList arrayUsersName,  int count_child, String search){
		this.layout = layout; this.layout_child = layout_child; this.json = json; this.count_child = count_child; this.c = c;
		this.arrayUsersImage = arrayUsersImage; this.arrayUsersName = arrayUsersName; this.imagePage = imagePage; this.jsonPage = jsonPage;
		this.search = search;
		
		d = new DisplayImageOptions.Builder().cacheInMemory().build();
		
		config = new ImageLoaderConfiguration.Builder(c).defaultDisplayImageOptions(d).build(); img = ImageLoader.getInstance();
		img.init(config);
	}
	
	
	
	@Override
	public boolean areAllItemsEnabled() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		LayoutInflater inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = inflater.inflate(layout_child, parent, false);
		
		image = (ImageView)rowView.findViewById(R.id.picture);
		name = (TextView)rowView.findViewById(R.id.name_voted);
		votes = (ProgressBar)rowView.findViewById(R.id.vote_voted);
		
		/*image.setClickable(false);
		image.setFocusable(false);
        image.setLongClickable(false);;
        image.setFocusableInTouchMode(false);*/
		
		votes.setVisibility(View.INVISIBLE);
		
		if(isLastChild && groupPosition!=0){
			System.out.println("utlimo");
			try {
				new VoteVIP.AsyncTaskJSON(c, search, false, arrayUsersImage, arrayUsersName,VoteVIP.usersJSON.getJSONObject("paging").getString("next")).execute();	
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			//do the reload of the expandable listview
		}
		
		try {
		
		   if(groupPosition==0){
			   img.displayImage(imagePage.getJSONObject("data").getString("url"), image);
				//image.loadUrl(imagePage.getJSONObject("data").getString("url"));
				name.setText(jsonPage.getJSONArray("data").getJSONObject(0).getString("name"));
			
		   }else{
			   
			   JSONObject image_url = (JSONObject)arrayUsersImage.get(childPosition);
			   
			   img.displayImage(image_url.getJSONObject("data").getString("url"), image);
			    //image.loadUrl(image_url.getJSONObject("data").getString("url"));
				name.setText(arrayUsersName.get(childPosition).toString().split("!")[0]);
			   
		   }
		
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return rowView;
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		// TODO Auto-generated method stub
		if(groupPosition==0){
			return 1;
		}else{
		    return arrayUsersName.size();
		}
	}

	@Override
	public long getCombinedChildId(long groupId, long childId) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public long getCombinedGroupId(long groupId) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Object getGroup(int groupPosition) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getGroupCount() {
		// TODO Auto-generated method stub
		return 2;
	}

	@Override
	public long getGroupId(int groupPosition) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		LayoutInflater inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = inflater.inflate(layout, parent, false);
		
		groupText = (TextView)rowView.findViewById(R.id.groupText);
		
		if(groupPosition==0){
			groupText.setText(c.getString(R.string.famous_person));
		}else{
			groupText.setText(c.getString(R.string.normal_persons));
		}
		
		return rowView;
	}

	@Override
	public boolean hasStableIds() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void onGroupCollapsed(int groupPosition) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onGroupExpanded(int groupPosition) {
		// TODO Auto-generated method stub
	}

	@Override
	public void registerDataSetObserver(DataSetObserver observer) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void unregisterDataSetObserver(DataSetObserver observer) {
		// TODO Auto-generated method stub
		
	}

}
