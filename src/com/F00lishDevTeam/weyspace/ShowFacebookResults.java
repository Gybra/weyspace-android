package com.F00lishDevTeam.weyspace;

import org.holoeverywhere.ArrayAdapter;
import org.holoeverywhere.app.Activity;
import org.holoeverywhere.app.Dialog;
import org.json.JSONException;
import org.json.JSONObject;

import org.holoeverywhere.app.AlertDialog;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.SpinnerAdapter;

import org.holoeverywhere.widget.AdapterView;
import org.holoeverywhere.widget.AdapterView.OnItemSelectedListener;
import org.holoeverywhere.widget.Button;
import org.holoeverywhere.widget.EditText;
import org.holoeverywhere.widget.ExpandableListView;
import org.holoeverywhere.widget.ExpandableListView.OnChildClickListener;
import org.holoeverywhere.widget.Spinner;

import com.lib.asyncHTTP.AsyncOperations;

public class ShowFacebookResults extends Activity{
	public static ExpandableListView expandable_results;
	static SharedPreferences sp;
	static int childPos=-1;
	private static AlertDialog.Builder alert;
	private Dialog dialog;
	private EditText choose_credits_bid;
	public static int groupPositionGlobal,childPositionGlobal;
	private int credits;
	private Activity act;
	private Button ok_credits;
	
	
	@Override
	public void onCreate(Bundle b){
		super.onCreate(b);
		
		setContentView(R.layout.results_facebook);
		
		act = this;
		
		sp = this.getSharedPreferences("login", MODE_PRIVATE);
		
		this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		
		expandable_results = (ExpandableListView)findViewById(R.id.expand_results);
		
		expandable_results.setAdapter(new ExpandableAdapaterCustom(this,R.layout.single_text_expandable,
				R.layout.list_voted,VoteVIP.imagePage,VoteVIP.pagesJSON,VoteVIP.arrayUsersImage,VoteVIP.arrayUsersName,1,
				this.getIntent().getStringExtra("searched")));
		
		expandable_results.expandGroup(0);
		expandable_results.expandGroup(1);
	
		expandable_results.setOnChildClickListener(new OnChildClickListener(){

			


			@Override
			public boolean onChildClick(ExpandableListView arg0, View arg1,
					int groupPosition, int childPosition, long arg4) {
				// TODO Auto-generated method stub
				/*AsyncOperations.putOnDB("http://95.110.196.250/weyspace/put_a_vote.php", new String[]{"nome","email","udid","pic"}, 
				new String[]{jobject.getJSONArray("data").getJSONObject(0).getString("name"),sp.getString("email", ""),jobject.getJSONArray("data")
				.getJSONObject(0).getString("id"),jobject2.getJSONObject("data").getString("url")});*/
				groupPositionGlobal = groupPosition; childPositionGlobal = childPosition;
				
				startActivity(new Intent(arg1.getContext(),VotePRSG.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK).putExtra("type", "statvip"));
				
		
		        return true;
			}
			
		});
		
	}
	
	
	
	@Override
	public boolean onOptionsItemSelected(com.actionbarsherlock.view.MenuItem item) {
		//this.startActivityForResult(new Intent(getApplicationContext(),mainTab.class),1);
		finish();
	    return true;
	}
	

}
