package com.F00lishDevTeam.weyspace;

import org.holoeverywhere.widget.TextView;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

public class load_countries extends ArrayAdapter<String>{

	private int layout;
	private Context c;
	private String[] names;
	private TextView flag_name;
	private ImageView flag_img;
	private String[] name_flags;
	private boolean global = false;
	
	public load_countries(Context context, int resource,
			int textViewResourceId, String[] objects) {
		super(context, resource, textViewResourceId, objects);
		
		c = context; layout = resource; names = objects; name_flags = c.getResources().getStringArray(R.array.flagship_name);
		// TODO Auto-generated constructor stub
	}
	
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) c
		        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		    View rowView = inflater.inflate(layout, parent, false);
		    
		flag_name = (TextView)rowView.findViewById(R.id.name_flag);
		flag_img = (ImageView)rowView.findViewById(R.id.nation_flag);
		
		flag_name.setText(names[position]);
		
		if(names[position].equals("Global")){
			global = true;
			flag_img.setVisibility(View.INVISIBLE);
		}else{
			if(global){
				flag_img.setImageResource(c.getResources().getIdentifier(name_flags[position-1], "drawable", c.getPackageName()));	
			}else{
				flag_img.setImageResource(c.getResources().getIdentifier(name_flags[position], "drawable", c.getPackageName()));
			}
		}
		
		    
		    
		return rowView;
	}

}
