package com.F00lishDevTeam.weyspace.sign_or_log;

import java.io.IOException;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;

import com.lib.asyncHTTP.*;
import com.F00lishDevTeam.weyspace.R;
import com.F00lishDevTeam.weyspace.VoteVIP;
import com.F00lishDevTeam.weyspace.load_countries;
import com.F00lishDevTeam.weyspace.mainTab;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import org.holoeverywhere.app.Activity;
import org.holoeverywhere.app.AlertDialog;
import org.holoeverywhere.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ArrayAdapter;

import org.holoeverywhere.widget.CheckBox;
import org.holoeverywhere.widget.DatePicker;
import org.holoeverywhere.widget.EditText;
import org.holoeverywhere.widget.ListView;
import org.holoeverywhere.widget.RadioButton;
import org.holoeverywhere.widget.TextView;

/**
 * Activity which displays a login screen to the user, offering registration as
 * well.
 */
public class MainActivity extends Activity {
	/**
	 * A dummy authentication store containing known user names and passwords.
	 * TODO: remove after connecting to a real authentication system.
	 */
	@SuppressWarnings("unused")
	private static final String[] DUMMY_CREDENTIALS = new String[] {
			"foo@example.com:hello", "bar@example.com:world" };

	/**
	 * The default email to populate the email field with.
	 */
	public static final String EXTRA_EMAIL = "com.example.android.authenticatordemo.extra.EMAIL";

	/**
	 * Keep track of the login task to ensure we can cancel it if requested.
	 */
	private UserLoginTask mAuthTask = null;

	// Values for email and password at the time of the login attempt.
	private String mEmail;
	private String mPassword;

	// UI references.
	private EditText mEmailView;
	private EditText mPasswordView;
	private EditText mSurnameView;
	private EditText mNameView;
	private RadioButton male,female;
	private DatePicker birth;
	private View mLoginFormView;
	private View mLoginStatusView;
	private TextView mLoginStatusMessageView;
	private SharedPreferences sp;
	private Dialog dialog;
	private ListView list;
	private String countrySelected="";
	private CheckBox read_policy;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		//this.setTheme(org.holoeverywhere.R.style.Holo_Theme_Light);
		
		setContentView(R.layout.register);
		
		GetJSONTask g = new GetJSONTask();
		g.execute();
		
		// Set up the login form.
		mEmail = getIntent().getStringExtra(EXTRA_EMAIL);
		mEmailView = (EditText) findViewById(R.id.email);
		mEmailView.setText(mEmail);

		mPasswordView = (EditText) findViewById(R.id.password);
		
		mLoginFormView = findViewById(R.id.login_form);
		mLoginStatusView = findViewById(R.id.login_status);
		mLoginStatusMessageView = (TextView) findViewById(R.id.login_status_message);
		read_policy = (CheckBox)findViewById(R.id.policy);

		findViewById(R.id.sign_in_button).setOnClickListener(
				new View.OnClickListener() {
					@Override
					public void onClick(View view) {
						attemptLogin();
					}
		});
		
		mNameView = (EditText)findViewById(R.id.name);
		
		
		
        findViewById(R.id.read_policy).setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				dialog = new Dialog(v.getContext(),R.style.Theme_Sherlock_Light_Dialog);
				dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
				dialog.setContentView(R.layout.web_dialog);
				WebView web = (WebView)dialog.findViewById(R.id.policy_web);
				
				if(Locale.getDefault().getLanguage().equals("zh")){
					web.loadUrl("http://docs.google.com/gview?embedded=true&url=http://95.110.196.250/weyspace/china_reg.doc");
				}else{
					if(Locale.getDefault().getLanguage().equals("it")){
						web.loadUrl("http://docs.google.com/gview?embedded=true&url=http://95.110.196.250/weyspace/it_reg.doc");
					}else{
						if(Locale.getDefault().getLanguage().equals("fr")){
							web.loadUrl("http://docs.google.com/gview?embedded=true&url=http://95.110.196.250/weyspace/fr_reg.doc");
						}else{
							if(Locale.getDefault().getLanguage().equals("ar")){
								web.loadUrl("http://docs.google.com/gview?embedded=true&url=http://95.110.196.250/weyspace/arab_reg.doc");
							}else{
								if(Locale.getDefault().getLanguage().equals("ko")){
									web.loadUrl("http://docs.google.com/gview?embedded=true&url=http://95.110.196.250/weyspace/kor_reg.doc");
								}else{
									web.loadUrl("http://docs.google.com/gview?embedded=true&url=http://95.110.196.250/weyspace/REGOLAMENTO.doc");
								}
								
							}
						}
					}
				}
				dialog.show();
				dialog.cancel();
			}
        });
		
		
		
		findViewById(R.id.nation).setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//startActivity(new Intent(getApplicationContext(),CountriesList.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
				dialog = new Dialog(v.getContext(),R.style.Theme_Sherlock_Light_Dialog);
				dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
				dialog.setContentView(R.layout.country_list);
				list = (ListView)dialog.findViewById(R.id.country_list_id);
				/*list.setAdapter(new ArrayAdapter<String>(v.getContext()
						, android.R.layout.simple_list_item_1, getResources().getStringArray(R.array.countries)));*/
				
				list.setAdapter(new load_countries(v.getContext(),R.layout.load_countries,android.R.id.text1,getResources().getStringArray(R.array.countries)));
				
				list.setOnItemClickListener(new OnItemClickListener(){

					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1,
							int arg2, long arg3) {
						// TODO Auto-generated method stub
						countrySelected = list.getItemAtPosition(arg2).toString();
						dialog.cancel();
					}
					
				});
				dialog.show();
			}
		});
		
		male = (RadioButton)findViewById(R.id.male);
		female = (RadioButton)findViewById(R.id.female);
		birth = (DatePicker)findViewById(R.id.birth);
		
		male.setOnCheckedChangeListener(new OnCheckedChangeListener(){

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				// TODO Auto-generated method stub
				
				if(isChecked){
					female.setChecked(false);
				}
				
			}
			
		});
		
		female.setOnCheckedChangeListener(new OnCheckedChangeListener(){

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				// TODO Auto-generated method stub
				if(isChecked){
					male.setChecked(false);
				}
			}
			
		});
		
		mSurnameView = (EditText)findViewById(R.id.surname);
		mSurnameView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
			

			@Override
			public boolean onEditorAction(android.widget.TextView v,
					int id, KeyEvent event) {
				// TODO Auto-generated method stub
				if (id == R.id.login || id == EditorInfo.IME_NULL) {
					attemptLogin();
					return true;
				}
				return false;
			}
		});
		
		
	}
	
	@Override
	protected void onActivityResult (int requestCode, int resultCode, Intent data){
		finish();
	}

	@Override
	public boolean onMenuItemSelected (int featureId, com.actionbarsherlock.view.MenuItem item){
		
		switch(item.getItemId()){
		  case R.id.login_item_jump:
			  startActivityForResult(new Intent(getApplicationContext(),LoginActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).putExtra("verification", true),1);
			break;
		}
		
		return true;
	}
	
	@Override
	public boolean onCreateOptionsMenu(com.actionbarsherlock.view.Menu menu) {
		super.onCreateOptionsMenu(menu);
		this.getSupportMenuInflater().inflate(R.menu.activity_login, menu);
		return true;
	}

	/**
	 * Attempts to sign in or register the account specified by the login form.
	 * If there are form errors (invalid email, missing fields, etc.), the
	 * errors are presented and no actual login attempt is made.
	 */
	public void attemptLogin() {
		if (mAuthTask != null) {
			return;
		}

		// Reset errors.
		mEmailView.setError(null);
		mPasswordView.setError(null);
		mSurnameView.setError(null);
		mNameView.setError(null);

		// Store values at the time of the login attempt.
		mEmail = mEmailView.getText().toString();
		mPassword = mPasswordView.getText().toString();

		boolean cancel = false;
		View focusView = null;

		// Check for a valid password.
		if (TextUtils.isEmpty(mPassword)) {
			mPasswordView.setError(getString(R.string.error_field_required));
			focusView = mPasswordView;
			cancel = true;
		} else if (mPassword.length() < 4) {
			mPasswordView.setError(getString(R.string.error_invalid_password));
			focusView = mPasswordView;
			cancel = true;
		}

		// Check for a valid email address.
		if (TextUtils.isEmpty(mEmail)) {
			mEmailView.setError(getString(R.string.error_field_required));
			focusView = mEmailView;
			cancel = true;
		} else if (!mEmail.contains("@")) {
			mEmailView.setError(getString(R.string.error_invalid_email));
			focusView = mEmailView;
			cancel = true;
		}
		
		if(TextUtils.isEmpty(mSurnameView.getText().toString())){
			mSurnameView.setError(getString(R.string.error_field_required));
			focusView = mSurnameView;
			cancel = true;
		}
		
		if(TextUtils.isEmpty(mNameView.getText().toString())){
			mNameView.setError(getString(R.string.error_field_required));
			focusView = mNameView;
			cancel = true;
		}
		
		if(TextUtils.isEmpty(countrySelected)){
			AlertDialog.Builder alert = new AlertDialog.Builder(this);
			alert.setTitle(getString(R.string.alert_title));
			alert.setMessage(getString(R.string.erro_fill_country));
			alert.setPositiveButton(getString(R.string.ok_alert), null);
			alert.show();
			focusView = findViewById(R.id.nation);
			cancel = true;
		}
		
		if(!read_policy.isChecked()){
			read_policy.setError(getString(R.string.error_field_required));
			focusView = read_policy;
			cancel = true;
		}

		if (cancel) {
			// There was an error; don't attempt login and focus the first
			// form field with an error.
			focusView.requestFocus();
		} else {
			// Show a progress spinner, and kick off a background task to
			// perform the user login attempt.
			mLoginStatusMessageView.setText(R.string.login_progress_signing_in);
			showProgress(true);
			mAuthTask = new UserLoginTask();
			mAuthTask.execute((Void) null);
		}
	}

	/**
	 * Shows the progress UI and hides the login form.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
	private void showProgress(final boolean show) {
		// On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
		// for very easy animations. If available, use these APIs to fade-in
		// the progress spinner.
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
			int shortAnimTime = getResources().getInteger(
					android.R.integer.config_shortAnimTime);

			mLoginStatusView.setVisibility(View.VISIBLE);
			mLoginStatusView.animate().setDuration(shortAnimTime)
					.alpha(show ? 1 : 0)
					.setListener(new AnimatorListenerAdapter() {
						@Override
						public void onAnimationEnd(Animator animation) {
							mLoginStatusView.setVisibility(show ? View.VISIBLE
									: View.GONE);
						}
					});

			mLoginFormView.setVisibility(View.VISIBLE);
			mLoginFormView.animate().setDuration(shortAnimTime)
					.alpha(show ? 0 : 1)
					.setListener(new AnimatorListenerAdapter() {
						@Override
						public void onAnimationEnd(Animator animation) {
							mLoginFormView.setVisibility(show ? View.GONE
									: View.VISIBLE);
						}
					});
		} else {
			// The ViewPropertyAnimator APIs are not available, so simply show
			// and hide the relevant UI components.
			mLoginStatusView.setVisibility(show ? View.VISIBLE : View.GONE);
			mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
		}
	}
	
	/**
	 * Represents an asynchronous login/registration task used to authenticate
	 * the user.
	 */
	public class GetJSONTask extends AsyncTask<Void, Void, JSONArray> {
		@Override
		protected JSONArray doInBackground(Void... params) {
			// TODO: attempt authentication against a network service.

			sp = getSharedPreferences("login", MODE_PRIVATE);
			
			// TODO: register the new account here.
			try {
				System.out.println("email "+sp.getString("email", ""));
				if(!sp.getString("email", "").equals("")){
					return AsyncOperations.readMyJSON("http://95.110.196.250/weyspace/json.php", new String[]{"email"}, new String[]{sp.getString("email", "")});	
				}else{
					return new JSONArray();
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				return new JSONArray();
			}
		}

		@Override
		protected void onPostExecute(final JSONArray resp) {
			
			sp = getSharedPreferences("login",MODE_PRIVATE);
			
			try {
				if(resp.length()!=0){
					if(resp.getJSONObject(0).getString("Enable").equalsIgnoreCase("off") && !sp.getBoolean("login", false)){
						AlertDialog.Builder alert = new AlertDialog.Builder(mLoginStatusView.getContext());
						alert.setMessage(getResources().getText(R.string.alert_message_verify));
						alert.setTitle(getResources().getText(R.string.alert_title));
						alert.setPositiveButton(getResources().getText(R.string.ok_alert), new OnClickListener(){
							
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								// TODO Auto-generated method stub
								
								finish();
								
							}
						});
						alert.show();
					}else{
						if(!resp.getJSONObject(0).getString("Enable").equalsIgnoreCase("off") && !sp.getBoolean("login", false)){
							VoteVIP.requestBackup(getApplicationContext());
							//startActivityForResult(new Intent(getApplicationContext(),LoginActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).putExtra("verification", true),1);
						}else{
							if(resp.getJSONObject(0).getString("Enable").equalsIgnoreCase("on") && sp.getBoolean("login", false)){
								//startActivityForResult(new Intent(getApplicationContext(),mainTab.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP),1);	
							}
						}
						//startActivity(new Intent(getApplicationContext(),mainTab.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
					}	
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			//startActivity(new Intent(getApplicationContext(),mainTab.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
			
		}
	}

	/**
	 * Represents an asynchronous login/registration task used to authenticate
	 * the user.
	 */
	public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {
		@Override
		protected Boolean doInBackground(Void... params) {
			// TODO: attempt authentication against a network service.

			
			
			

			// TODO: register the new account here.
			return AsyncOperations.putOnDB("http://95.110.196.250/weyspace/put_a_user.php", new String[]{"nazione",
					"password","birth","gender","email","name","surname"}, new String[]{
					countrySelected,mPasswordView.getText().toString(),birth.getDayOfMonth()+"/"+birth.getMonth()+"/"+birth.getYear(),
					male.isChecked() ? "M" : "F", mEmailView.getText().toString(), mNameView.getText().toString(), mSurnameView.getText().toString()});
		}

		@Override
		protected void onPostExecute(final Boolean success) {
			mAuthTask = null;
			showProgress(false);
			
			if(success){
				sp = getSharedPreferences("login",MODE_PRIVATE);
				Editor e = sp.edit();
				e.putString("email", mEmailView.getText().toString());
				e.putString("nation", countrySelected);
				e.putBoolean("login", false);
				e.commit();
				startActivity(new Intent(getApplicationContext(),LoginActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).putExtra("verification", false));
			}else{
				AlertDialog.Builder alert = new AlertDialog.Builder(mLoginStatusView.getContext());
				alert.setMessage(getResources().getText(R.string.alert_message));
				alert.setTitle(getResources().getText(R.string.alert_title));
				alert.setPositiveButton(getResources().getText(R.string.ok_alert), null);
				alert.show();
			}
			
		
		}

		@Override
		protected void onCancelled() {
			mAuthTask = null;
			showProgress(false);
		}
	}
}
