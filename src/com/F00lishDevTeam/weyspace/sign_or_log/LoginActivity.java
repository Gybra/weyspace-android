package com.F00lishDevTeam.weyspace.sign_or_log;

import com.F00lishDevTeam.weyspace.R;
import com.F00lishDevTeam.weyspace.VoteVIP;
import com.F00lishDevTeam.weyspace.mainTab;
import com.actionbarsherlock.app.SherlockActivity;
import com.lib.asyncHTTP.AsyncOperations;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

public class LoginActivity extends SherlockActivity{
	
	private EditText username,password;
	private SharedPreferences sp;
	private Editor e;
	
	@Override
	public void onCreate(Bundle b){
		super.onCreate(b);
		
		setContentView(R.layout.login);
		
		this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		
		if(!getIntent().getBooleanExtra("verification", false)){
			AlertDialog.Builder alert = new AlertDialog.Builder(this);
			alert.setMessage(getResources().getText(R.string.alert_message_verify));
			alert.setTitle(getResources().getText(R.string.alert_title));
			alert.setPositiveButton(getResources().getText(R.string.ok_alert), null);
			
			
			alert.show();
		}
		
		
		sp = this.getSharedPreferences("login", MODE_PRIVATE);
		e = sp.edit();//errore non viene salvato
		username = (EditText)findViewById(R.id.username_log);
		password = (EditText)findViewById(R.id.password_log);
		
		password.setOnEditorActionListener(new TextView.OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView textView, int id,
					KeyEvent keyEvent) {
				if (id == R.id.plogin || id == EditorInfo.IME_NULL) {
					
					if(username.getText().toString().length()==0){
						username.setError(getString(R.string.error_field_required));
					}
					
					if(password.getText().toString().length()==0){
						password.setError(getString(R.string.error_field_required));
					}
					
					if(password.getText().toString().length()!=0 && username.getText().toString().length()!=0){
						new LoginTask().execute();
					}
					
					
					return true;
				}
				return false;
			}
		});
		
		findViewById(R.id.Login).setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(username.getText().toString().length()==0){
					username.setError(getString(R.string.error_field_required));
				}
				
				if(password.getText().toString().length()==0){
					password.setError(getString(R.string.error_field_required));
				}
				
				if(password.getText().toString().length()!=0 && username.getText().toString().length()!=0){
					new LoginTask().execute();
				}
			}
			
		});
		
	}
	
	@Override
	public boolean onOptionsItemSelected(com.actionbarsherlock.view.MenuItem item) {
		this.startActivity(new Intent(getApplicationContext(),MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
	    return true;
	}
	
	public class LoginTask extends AsyncTask<Void,Void,Boolean>{

		@Override
		protected Boolean doInBackground(Void... params) {
			// TODO Auto-generated method stub
			//e.putString(key, value)
			
			try {
				e.putString("nation", AsyncOperations.readMyJSON("http://95.110.196.250/weyspace/json.php", 
						new String[]{"email"} , new String[]{username.getText().toString()}).getJSONObject(0).getString("Nation"));
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			return AsyncOperations.putOnDB("http://95.110.196.250/weyspace/login.php", new String[]{"email","password"}, new String[]{username.getText().toString(),
					password.getText().toString()});
		}
		
		@Override
		protected void onPostExecute(final Boolean success) {
			if(success){
				e.putString("email", username.getText().toString());
				e.putBoolean("login", true);
				e.commit();
				VoteVIP.requestBackup(getApplicationContext());
				startActivityForResult(new Intent(getApplicationContext(),mainTab.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP),1);
			}else{
				username.setError(getString(R.string.error_incorrect_username));
				password.setError(getString(R.string.error_incorrect_password));
			}
		}
		
	}
	
	@Override
	protected void onActivityResult (int requestCode, int resultCode, Intent data){
		finish();
	}

}
