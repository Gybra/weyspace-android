package com.F00lishDevTeam.weyspace.sign_or_log;

import org.holoeverywhere.app.Activity;

import com.F00lishDevTeam.weyspace.R;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

public class Splash extends Activity{
	
	private Handler mHandler;
	private Thread t;
	
	public void onCreate(Bundle b){
		super.onCreate(b);
		
		setContentView(R.layout.splash_screen);
		
		this.getSupportActionBar().hide();
		
		mHandler = new Handler();
		
		t = new Thread(new Runnable() {
	        @Override
	        public void run() {
	            // TODO Auto-generated method stub
	            //while (true) {
	                try {
	                	Thread.sleep(1000);
	                    mHandler.post(new Runnable() {

	                        @Override
	                        public void run() {
	                            // TODO Auto-generated method stub
	                            // Write your code here to update the UI.
	                        
	                        	startActivityForResult(new Intent(getApplicationContext(),MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP),1);
	                        }
	                    });
	                } catch (Exception e) {
	                    // TODO: handle exception
	                }
	            //}
	        }
	    });
		
		t.start();
	}
	
	@Override
	protected void onActivityResult (int requestCode, int resultCode, Intent data){
		finish();
	}

}
