package com.F00lishDevTeam.weyspace;

import java.util.Locale;

import org.holoeverywhere.app.Activity;
import org.holoeverywhere.widget.LinearLayout;


import android.os.Bundle;
import android.os.Handler;

public class Third extends Activity{
	private Thread t;
	private Handler mHandler;
	private LinearLayout lay;
	private boolean first = false;
	
	
	public void onCreate(Bundle b){
		super.onCreate(b);
		
		setContentView(R.layout.third);
		
		
		lay = (LinearLayout)findViewById(R.id.lay_third);
		
		mHandler = new Handler();
		
		t = new Thread(new Runnable() {
	        @Override
	        public void run() {
	            // TODO Auto-generated method stub
	            while (true) {
	                try {
	                	Thread.sleep(2000);
	                    mHandler.post(new Runnable() {

	                        @Override
	                        public void run() {
	                            // TODO Auto-generated method stub
	                            // Write your code here to update the UI.
	                        	if(!first){
	                        		first = true;
	                        		lay.setBackgroundResource(R.drawable.sfondo2);	
	                        	}else{
	                        		first = false;
	                        		if(Locale.getDefault().getLanguage().equals("zh")){
	                        			lay.setBackgroundResource(R.drawable.cinese);
	                        		}else{
	                        			if(Locale.getDefault().getLanguage().equals("it")){
		                        			lay.setBackgroundResource(R.drawable.italiano);
		                        		}else{
		                        			if(Locale.getDefault().getLanguage().equals("fr")){
			                        			lay.setBackgroundResource(R.drawable.francese);
			                        		}else{
			                        			if(Locale.getDefault().getLanguage().equals("ar")){
			                        				lay.setBackgroundResource(R.drawable.arabic);
			                        			}else{
			                        				if(Locale.getDefault().getLanguage().equals("ko")){
			                        					//lay.setBackgroundResource(R.drawable.i); koreano
			                        				}else{
			                        					lay.setBackgroundResource(R.drawable.inglese);
			                        				}
			                        				
			                        			}
			                        			
			                        		}
		                        		}
	                        		}
	                        		
	                        		
	                        	}
	                        }
	                    });
	                } catch (Exception e) {
	                    // TODO: handle exception
	                }
	            }
	        }
	    });
		
		
		t.start();
		
	}

}
