package com.F00lishDevTeam.weyspace;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;

import com.lib.asyncHTTP.AsyncOperations;

import org.holoeverywhere.app.Activity;
import org.holoeverywhere.app.AlertDialog;
import org.holoeverywhere.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import org.holoeverywhere.widget.Button;
import org.holoeverywhere.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

public class SatsVote extends Activity{
	
	private ListView list,list_dialog;
	private Button country;
	private Dialog dialog;
	private String countrySelected;
	private AlertDialog.Builder alert;
	private Context c;
	
	public static ArrayList array;
	
	@Override
	public void onCreate (Bundle savedInstanceState)
	{
	  super.onCreate(savedInstanceState);
	  
	  setContentView(R.layout.statsvote);
	  
	  list = (ListView)findViewById(R.id.nations_list);
	  country = (Button)findViewById(R.id.countries_list);
	  c = this;
	  
	  countrySelected = "Global";
	  
	  loadStat();
	  
	}
	
	private void loadStat(){
		new LoadStats(this).execute();
		  
		  country.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog = new Dialog(v.getContext(),R.style.Theme_Sherlock_Light_Dialog);
				dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
				dialog.setContentView(R.layout.country_list);
				list_dialog = (ListView)dialog.findViewById(R.id.country_list_id);
				/*list_dialog.setAdapter(new ArrayAdapter<String>(v.getContext()
						, android.R.layout.simple_list_item_1, getResources().getStringArray(R.array.countries_plus_global)));*/
				list_dialog.setAdapter(new load_countries(v.getContext(),R.layout.load_countries,android.R.id.text1,getResources().getStringArray(R.array.countries_plus_global)));
				
				list_dialog.setOnItemClickListener(new OnItemClickListener(){

					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1,
							int arg2, long arg3) {
						// TODO Auto-generated method stub
						countrySelected = list_dialog.getItemAtPosition(arg2).toString();
						dialog.cancel();
						country.setText(countrySelected);
						
						new LoadStats(c).execute();
					}
					
				});
				dialog.show();
			}
			  
		  });
	}
	
	public void onResume(){
		super.onResume();
		
		loadStat();
	}

    public boolean onMenuItemSelected (int featureId, com.actionbarsherlock.view.MenuItem item){
		
		switch(item.getItemId()){
		  case R.id.menu_forgot_password:
			  startActivity(new Intent(getApplicationContext(),Recovery.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
			break;
			
		  case R.id.send_email:
			  Intent intent = new Intent (Intent.ACTION_VIEW , Uri.parse("mailto:" + "help@weyspace.com"));
			  intent.putExtra(Intent.EXTRA_SUBJECT, "Help!");
			  startActivity(intent); 
			  
			  break;
		}
		
		return true;
	}
	
	public boolean onCreateOptionsMenu(com.actionbarsherlock.view.Menu menu) {
		super.onCreateOptionsMenu(menu);
		this.getSupportMenuInflater().inflate(R.menu.recovery, menu);
		return true;
	}
	
	
	private class LoadStats extends AsyncTask<Void,Void,JSONArray>{
		Context c;
		LoadStats(Context c){
			this.c = c;
		}
		
		@Override
		protected JSONArray doInBackground(Void... params) {
			// TODO Auto-generated method stub
			System.out.println("t "+countrySelected);
			if(countrySelected.equalsIgnoreCase("Global")){
				try {
					return AsyncOperations.readMyJSON("http://95.110.196.250/weyspace/json.php", new String[]{}, new String[]{});
				} catch (Exception e) {
					// TODO Auto-generated catch block
					return new JSONArray();
				}
			}else{
				System.out.println("entrato");
				try {
					return AsyncOperations.readMyJSON("http://95.110.196.250/weyspace/json.php", new String[]{"nation"}, new String[]{countrySelected});
				} catch (Exception e) {
					// TODO Auto-generated catch block
					return new JSONArray();
				}	
			}
		}
		
		@Override
		protected void onPostExecute(JSONArray json) {
			if(json.length()!=0){
				array = new ArrayList();
	        	try {
	        		for(int i=0;i<json.length();i++){
	        			array.add(json.getJSONObject(i));
	        		}
	        		list.setAdapter(new ListVotedStats(c,R.layout.list_voted,R.id.name_voted, array,json));
	        		
	        		list.setOnItemClickListener(new OnItemClickListener(){

						@Override
						public void onItemClick(AdapterView<?> arg0, View arg1,
								int arg2, long arg3) {
							// TODO Auto-generated method stub
							startActivity(new Intent(arg1.getContext(),VotePRSG.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK).putExtra("type", "globvip")
									.putExtra("position", arg2));
						}
						
					});
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}else{
				alert = new AlertDialog.Builder(c);
				alert.setTitle(R.string.title_dialog_go_with);
				alert.setMessage(getString(R.string.error_alert_connections));
				alert.setPositiveButton(getString(R.string.ok_alert), null);
				alert.show();
				list.setAdapter(new ListVoted(c,R.layout.list_voted,R.id.name_voted, new ArrayList(),new JSONArray()));
			}
		}
		
	}
	
}
