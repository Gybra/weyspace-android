package com.F00lishDevTeam.weyspace;

import java.util.ArrayList;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.F00lishDevTeam.weyspace.in_app.util.IabHelper;
import com.F00lishDevTeam.weyspace.in_app.util.IabResult;
import com.F00lishDevTeam.weyspace.in_app.util.Purchase;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.model.GraphUser;
import com.lib.asyncHTTP.AsyncOperations;

import org.holoeverywhere.app.Activity;
import org.holoeverywhere.app.AlertDialog;
import org.holoeverywhere.app.Dialog;

import android.app.backup.BackupManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.EditorInfo;

import org.holoeverywhere.widget.AdapterView;
import org.holoeverywhere.widget.Button;
import org.holoeverywhere.widget.EditText;
import org.holoeverywhere.widget.ExpandableListView;
import org.holoeverywhere.widget.ListView;
import org.holoeverywhere.widget.TextView;

import android.widget.AdapterView.OnItemClickListener;
import android.widget.TextView.OnEditorActionListener;

public class VoteVIP extends Activity{
	private Button vip,buy_credits;
	private EditText search;
	private static Activity act;
	private static SharedPreferences sp;
	public static ListView list;
	private static Editor e;
	private AlertDialog.Builder dialog;
	private ExpandableListView expandable_results;
	
	public static TextView credits;
	public static JSONObject pagesJSON,usersJSON,imagePage,imageUsers;
	public static ArrayList arrayUsersImage;
	public static ArrayList arrayUsersName;
	public static ArrayList array;
	
	@Override
	public void onCreate (Bundle savedInstanceState)
	{
	  super.onCreate(savedInstanceState);
	  
	  setContentView(R.layout.votevip);
	  act = this;
	  sp = this.getSharedPreferences("login", MODE_PRIVATE);
	  vip = (Button)this.findViewById(R.id.facebook);
	  search = (EditText)this.findViewById(R.id.name_search);
	  vip.setOnClickListener(new Listener());
	  list = (ListView)act.findViewById(R.id.voted);
	  buy_credits = (Button)act.findViewById(R.id.buy_more_credits);
	  credits = (TextView)this.findViewById(R.id.credits);
	  
	  //credits.setText("1300 credits");
	  
	  new AsyncCredits().execute();
	  //credits.setText(sp.getInt("credits", 0)+" "+getString(R.string.credits));
	  logInToFacebook();
	  
	  
	  new AsyncTaskVotedList().execute();
	  
	  
	  list.setOnItemClickListener(new OnItemClickListener(){

			@Override
			public void onItemClick(android.widget.AdapterView<?> arg0,
					View arg1, int arg2, long arg3) {
				// TODO Auto-generated method stub
				//ListVoted.buyStuff(list,arg2);
				startActivity(new Intent(getApplicationContext(),VotePRSG.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK).putExtra("position", arg2)
						.putExtra("type", "votevip"));
			}
			
		});
	  
	  buy_credits.setOnClickListener(new OnClickListener(){

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			//new AsyncTaskBuy().execute();
			startActivity(new Intent(v.getContext(),ChooseBuyCredits.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK));
			
			/*mainTab.mHelper.launchPurchaseFlow(act, "crediti", 1001, 
	                mainTab.mPurchaseFinishedListener, "");*/
		}
		  
	  });
	  
	  
	  search.setOnEditorActionListener(new OnEditorActionListener(){

		@Override
		public boolean onEditorAction(android.widget.TextView v, int id, KeyEvent event) {
			// TODO Auto-generated method stub
			
			if (id == R.id.search_name || id == EditorInfo.IME_NULL) {
				search.setError(null);
				
				
				if(v.length()==0){
					search.setError(getString(R.string.error_field_required));
				}else{
					new AsyncTaskJSON(VoteVIP.this,search.getText().toString(),true,null,null,null).execute();	
				}
				
				return true;
			}
			
			return false;
		}
		  
	  });
	  
	}
	
	
	@Override
	public void onResume(){
		super.onResume();
		if(mainTab.voted){
			credits.setText(((int)Double.parseDouble(VoteVIP.credits.getText().toString().split(" ")[0])-mainTab.credits)+" "+getString(R.string.credits));
			Editor e = sp.edit();
        	e.putInt("credits", sp.getInt("credits", 0)-mainTab.credits);
        	e.commit();
        	VoteVIP.requestBackup(this);
			mainTab.voted = false;
			
		}
	}
	
	public static void logInToFacebook(){
		
		Session.openActiveSession(act, true, new Session.StatusCallback() {

		      // callback when session changes state
		      @Override
		      public void call(final Session session, SessionState state, Exception exception) {
		    	   if(session.isOpened()){
		    		    Request.executeMeRequestAsync(session, new Request.GraphUserCallback() {
							
							@Override
							public void onCompleted(GraphUser user, Response response) {
								// TODO Auto-generated method stub
								e = sp.edit();
								e.putString("username_facebook", user.getUsername());
								e.putString("access_token", session.getAccessToken());
				        		e.commit();
							}
						});
		    	   }
		      }

		 });
	}
	
	
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		//if (!mainTab.mHelper.handleActivityResult(requestCode, resultCode, data)) {
	      super.onActivityResult(requestCode, resultCode, data);
	      Session.getActiveSession().onActivityResult(this, requestCode, resultCode, data);
		//}
	}
	
	private class Listener implements OnClickListener{
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
				
			search.setError(null);
			
			if(TextUtils.isEmpty(search.getText().toString())){
				search.setError(getString(R.string.error_field_required));
			}else{
				//logInToFacebook(type);
				
				new AsyncTaskJSON(VoteVIP.this,search.getText().toString(),true,null,null,null).execute();
			}
		}
		
	}
	
	public static class AsyncTaskVotedList extends AsyncTask<Void,Void,String>{
		@Override
		protected String doInBackground(Void... params) {
			//list.setAdapter(new ListVoted());
			try {
				return AsyncOperations.readMyJSONGet("http://95.110.196.250/weyspace/json.php",null,null);
				//System.out.println("resp "+AsyncOperations.readMyJSONGet("http://95.110.196.250/weyspace/json.php",null,null));
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				return null;
			}
			
		}
		
        @Override
		protected void onPostExecute(String resp) {
			array = new ArrayList();
        	try {
        		JSONArray json = new JSONArray(resp);
        		for(int i=0;i<json.length();i++){
        			array.add(json.getJSONObject(i));
        		}
				list.setAdapter(new ListVoted(act.getApplicationContext(),R.layout.list_voted,R.id.name_voted, array,json));
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
		}
	}
	
	public static class AsyncTaskJSON extends AsyncTask<Void,Void,Boolean>{
		Context c;
		String search;
		boolean start;
		Dialog wait_dialog;
		String next_url;
		
		AsyncTaskJSON(Context c, String search, boolean start, ArrayList arrayUsersImage, ArrayList arrayUsersName, String next_url){
			this.c = c; this.search = search; this.start = start; 
			
			if(!start){
				VoteVIP.arrayUsersImage = arrayUsersImage; VoteVIP.arrayUsersName = arrayUsersName;
				this.next_url = next_url;
			}
		}
		
		@Override
		protected void onPreExecute(){
			wait_dialog = new Dialog(c,R.style.Theme_Sherlock_Light_Dialog);
			wait_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			wait_dialog.setContentView(R.layout.waiting_progress);
			wait_dialog.setCancelable(false);
			wait_dialog.show();
		}
		
		
		@Override
		protected Boolean doInBackground(Void... params) {
			// TODO Auto-generated method stub
			if(start){
				arrayUsersImage = new ArrayList();
				arrayUsersName = new ArrayList();	
			}
			try {
				pagesJSON = new JSONObject(AsyncOperations.readMyJSONGet("https://graph.facebook.com/search?q="+search
						.replaceAll(" ", "+")+"&type=page",null,null));
				
				imagePage = new JSONObject(AsyncOperations.readMyJSONGet("http://graph.facebook.com/"+pagesJSON.getJSONArray("data")
						.getJSONObject(0).getString("id")+"/picture?type=large&redirect=false",null,null));
				
				if(start){
					usersJSON = new JSONObject(AsyncOperations.readMyJSONGet("https://graph.facebook.com/search?q="+search
							.replaceAll(" ", "+")+"&type=user&access_token="+sp.getString("access_token", ""),null,null));
				}else{
					usersJSON = new JSONObject(AsyncOperations.readMyJSONGet(next_url,null,null));
				}
				
				for(int i=0;i<usersJSON.getJSONArray("data").length();i++){
					
					arrayUsersName.add(usersJSON.getJSONArray("data")
							.getJSONObject(i).getString("name")+"!"+usersJSON.getJSONArray("data")
							.getJSONObject(i).getString("id"));
					
					imageUsers = new JSONObject(AsyncOperations.readMyJSONGet("http://graph.facebook.com/"+usersJSON.getJSONArray("data")
							.getJSONObject(i).getString("id")+"/picture?redirect=false",null,null));	
					
					arrayUsersImage.add(imageUsers);
				}
				return true;
			} catch (Exception e) {
				// TODO Auto-generated catch block
				return false;
			}
			
		}
		
		@Override
		protected void onPostExecute(Boolean success) {
			wait_dialog.cancel();
			if(success){
				
				if(start){
				  c.startActivity(new Intent(c,ShowFacebookResults.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
						.putExtra("searched", search));
				}else{
					Parcelable parce = ShowFacebookResults.expandable_results.onSaveInstanceState();
					
					ShowFacebookResults.expandable_results.invalidateViews();
					ShowFacebookResults.expandable_results.setAdapter(new ExpandableAdapaterCustom(c,R.layout.single_text_expandable,
							R.layout.list_voted,VoteVIP.imagePage,VoteVIP.pagesJSON,VoteVIP.arrayUsersImage,VoteVIP.arrayUsersName,1,
							search));
					
					ShowFacebookResults.expandable_results.expandGroup(0);
					ShowFacebookResults.expandable_results.expandGroup(1);
					
					ShowFacebookResults.expandable_results.onRestoreInstanceState(parce);
					
				}
				
			}else{
			    VoteVIP.logInToFacebook();
			}
			
		}
	}
	
	
	public static final void requestBackup(Context mContext){
		BackupManager b = new BackupManager(mContext);
		b.dataChanged();
		//Toast.makeText(mContext, �Backup richiesto�, Toast.LENGTH_SHORT).show();
    }
	
	
    public boolean onMenuItemSelected (int featureId, com.actionbarsherlock.view.MenuItem item){
		
		switch(item.getItemId()){
		  case R.id.menu_forgot_password:
			  startActivity(new Intent(getApplicationContext(),Recovery.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
			break;
			
		  case R.id.send_email:
			  Intent intent = new Intent (Intent.ACTION_VIEW , Uri.parse("mailto:" + "help@weyspace.com"));
			  intent.putExtra(Intent.EXTRA_SUBJECT, "Help!");
			  startActivity(intent); 
			  
			  break;
		}
		
		return true;
	}
	
	public boolean onCreateOptionsMenu(com.actionbarsherlock.view.Menu menu) {
		super.onCreateOptionsMenu(menu);
		this.getSupportMenuInflater().inflate(R.menu.recovery, menu);
		return true;
	}
	
	private class AsyncCredits extends AsyncTask<Void,Void,JSONArray>{

		@Override
		protected JSONArray doInBackground(Void... arg0) {
			// TODO Auto-generated method stub
			try {
				System.out.println("enteredx");
				return AsyncOperations.readMyJSON("http://95.110.196.250/weyspace/json.php", new String[]{"email","used"}, new String[]{sp.getString("email", ""),"yes"});
			} catch (Exception e) {
				// TODO Auto-generated catch block
				return new JSONArray();
			}
		}
		
		protected void onPostExecute(JSONArray json) {
			System.out.println("prova "+json.length());
			if(json.length()!=0){
				try {
					System.out.println("prova "+String.valueOf(json.getJSONObject(0).getInt("Credits")));
					credits.setText(String.valueOf(json.getJSONObject(0).getInt("Credits"))+" "+getString(R.string.credits));
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
	}

}
