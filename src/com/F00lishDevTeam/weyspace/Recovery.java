package com.F00lishDevTeam.weyspace;

import org.holoeverywhere.app.Activity;
import org.holoeverywhere.preference.SharedPreferences;
import org.holoeverywhere.widget.Button;
import org.holoeverywhere.widget.EditText;

import com.F00lishDevTeam.weyspace.sign_or_log.MainActivity;
import com.lib.asyncHTTP.AsyncOperations;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;

public class Recovery extends Activity{
	private EditText email,password;
	private Button recovery;
	private SharedPreferences sp;
	
	public void onCreate(Bundle b){
		super.onCreate(b);
		setContentView(R.layout.login);
		
		this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		
		email = (EditText)findViewById(R.id.username_log);
		email.setVisibility(email.INVISIBLE);
		
		password = (EditText)findViewById(R.id.password_log);
		recovery = (Button)findViewById(R.id.Login);
		
		sp = this.getSharedPreferences("login", MODE_PRIVATE);
		
		password.setHint(R.string.prompt_new_pass);
		
		recovery.setText(R.string.prompt_recovery);
		
		
		recovery.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				new RecoveryTask().execute();
			}
			
		});
		
		
	}
	
	public boolean onOptionsItemSelected(com.actionbarsherlock.view.MenuItem item) {
		finish();
	    return true;
	}
	

	
	private class RecoveryTask extends AsyncTask<Void,Void,Boolean>{

		@Override
		protected Boolean doInBackground(Void... params) {
			// TODO Auto-generated method stub
			return AsyncOperations.putOnDB("http://95.110.196.250/weyspace/change_password.php", new String[]{"email","password"}, new String[]{sp.getString("email", ""),password.getText().toString()});
		}
		
		protected void onPostExecute(Boolean success) {
			if(success){
				finish();
			}
		}
		
	}

}
