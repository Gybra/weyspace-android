package com.F00lishDevTeam.weyspace;


import com.F00lishDevTeam.weyspace.VoteVIP.AsyncTaskVotedList;
import com.F00lishDevTeam.weyspace.in_app.util.IabHelper;
import com.F00lishDevTeam.weyspace.in_app.util.IabResult;
import com.F00lishDevTeam.weyspace.in_app.util.Purchase;
import com.F00lishDevTeam.weyspace.sign_or_log.LoginActivity;
import com.facebook.Session;

import android.app.Activity;
import android.app.TabActivity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TabHost.OnTabChangeListener;





public class mainTab extends TabActivity{
	
	public static int childPosition,credits;
	public static boolean voted;
	
	
	private static Activity act;
	
	@Override
	public void onCreate(Bundle b){
		super.onCreate(b);
		
		//this.setTheme(org.holoeverywhere.R.style.Holo_Theme_Light_NoActionBar);
		
		/*voted = this.getIntent().getBooleanExtra("voted", false);
		childPosition = this.getIntent().getIntExtra("position", -2);
		credits = this.getIntent().getIntExtra("howmuch", 0);*/
		
		act = this;
		
	      
	    // enable debug logging (for a production application, you should set this to false).
	    //mHelper.enableDebugLogging(true);

	    // Start setup. This is asynchronous and the specified listener
	    // will be called once setup completes.
		
		
		
		
		this.getTabHost().addTab(this.getTabHost().newTabSpec("Vote").
				setIndicator("",this.getResources().getDrawable(R.drawable.votes_selector)).setContent(new Intent(getBaseContext(),VoteVIP.class)));
		
		this.getTabHost().addTab(this.getTabHost().newTabSpec("Stats").
				setIndicator("",this.getResources().getDrawable(R.drawable.stats_selector)).setContent(new Intent(getBaseContext(),SatsVote.class)));
		
		this.getTabHost().addTab(this.getTabHost().newTabSpec("Random Stats").
				setIndicator("",this.getResources().getDrawable(R.drawable.random_selector)).setContent(new Intent(getBaseContext(),Random.class)));
		
		this.getTabHost().addTab(this.getTabHost().newTabSpec("Third Tab").
				setIndicator("",this.getResources().getDrawable(R.drawable.third_selector)).setContent(new Intent(getBaseContext(),Third.class)));
		
		
		this.getTabHost().setOnTabChangedListener(new OnTabChangeListener(){

			@Override
			public void onTabChanged(String tabId) {
				// TODO Auto-generated method stub
				if(tabId.equals("Vote")){
					//System.out.println("eiii");
					new AsyncTaskVotedList().execute();
				}
			}
			
		});
		
	}
	
	
    public boolean onMenuItemSelected (int featureId, MenuItem item){
		
		switch(item.getItemId()){
		  case R.id.menu_forgot_password:
			  startActivity(new Intent(getApplicationContext(),Recovery.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
			break;
			
		  case R.id.send_email:
			  Intent intent = new Intent (Intent.ACTION_VIEW , Uri.parse("mailto:" + "help@weyspace.com"));
			  intent.putExtra(Intent.EXTRA_SUBJECT, "Help!");
			  startActivity(intent); 
			  
			  break;
		}
		
		return true;
	}
	
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		this.getMenuInflater().inflate(R.menu.recovery, menu);
		return true;
	}

}
