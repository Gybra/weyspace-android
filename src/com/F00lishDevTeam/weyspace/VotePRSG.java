package com.F00lishDevTeam.weyspace;

import org.holoeverywhere.app.Activity;
import org.holoeverywhere.app.AlertDialog;
import org.holoeverywhere.app.Dialog;
import org.holoeverywhere.widget.Button;
import org.holoeverywhere.widget.EditText;
import org.holoeverywhere.widget.TextView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.F00lishDevTeam.weyspace.VoteVIP.AsyncTaskVotedList;
import com.lib.asyncHTTP.AsyncOperations;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.ImageView;

public class VotePRSG extends Activity{
	
	private ImageLoaderConfiguration config;
	private ImageLoader img;
	private ImageView vote_pic,my_pic;
	private SharedPreferences sp;
	private Button vote,ok_credits;
	private Dialog dialog;
	private EditText choose_credits_bid;
	private int credits,childPos;
	private AlertDialog.Builder alert;
	private TextView name_vote;
	
	public void onCreate(Bundle b){
		super.onCreate(b);
		setContentView(R.layout.vote_prsg);
		
		this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		
		vote_pic = (ImageView)findViewById(R.id.vote_pic);
		my_pic = (ImageView)findViewById(R.id.my_pic);
		sp = this.getSharedPreferences("login", MODE_PRIVATE);
		vote = (Button)findViewById(R.id.vote);
		name_vote = (TextView)findViewById(R.id.name_vote);
		
		config = new ImageLoaderConfiguration.Builder(this).
				threadPoolSize(ImageLoaderConfiguration.Builder.DEFAULT_THREAD_POOL_SIZE).build(); img = ImageLoader.getInstance();
				
				
		new AsyncTaskGoWithJSON().execute();
		try {
			name_vote.setText(getName());
			img.displayImage(getImage(), vote_pic);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		vote.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				buyStuff(0);
			}
			
		});
		
	}
	
	
	private void buyStuff(final int type){
		if(!VoteVIP.credits.getText().toString().split(" ")[0].equals("0")){
			
			dialog = new Dialog(this);
        	dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        	dialog.setContentView(R.layout.select_credits_to_vote);
        	choose_credits_bid = (EditText)dialog.findViewById(R.id.select_credits);
        	ok_credits = (Button)dialog.findViewById(R.id.ok_credits);
			
			
			if(getIntent().getStringExtra("type").equals("votevip") || getIntent().getStringExtra("type").equals("globvip")){
				ok_credits.setOnClickListener(new OnClickListener(){

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
	                    if(choose_credits_bid.getText().length()>0){
							
							credits = Integer.parseInt(choose_credits_bid.getText().toString());
							
							dialog.cancel();
							
							if(Integer.parseInt(VoteVIP.credits.getText().toString().split(" ")[0])<credits){
								alert(v.getContext());
							}else{
								if(type!=0){
									credits = Integer.parseInt(choose_credits_bid.getText().toString());
									new AsyncTaskGoWith().execute();
								}else{
									childPos = getIntent().getIntExtra("position", -1);
									new AsyncTaskVote().execute();	
								}
							}
								
						}
					}
	        		
	        	});
			}else{
				if(getIntent().getStringExtra("type").equals("statvip")){
					ok_credits.setOnClickListener(new OnClickListener(){

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
		                    if(!choose_credits_bid.getText().toString().contains("Choose")){
								
								credits = Integer.parseInt(choose_credits_bid.getText().toString());
								
								dialog.cancel();
								
								if(Integer.parseInt(VoteVIP.credits.getText().toString().split(" ")[0])<credits){
									alert(v.getContext());
								}else{
									if(ShowFacebookResults.groupPositionGlobal==0){
										if(type!=0){
											new AsyncTaskGoWith().execute();
										}else{
											new AsyncTaskVote().execute();
										}
								    }else{
								    	if(type!=0){
								    		new AsyncTaskGoWith().execute();
								    	}else{
									      childPos = ShowFacebookResults.childPositionGlobal;
									      new AsyncTaskVote().execute();
								    	}
								    }	
								}
									
							}
						}
		        		
		        	});
				}else{
					ok_credits.setOnClickListener(new OnClickListener(){

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
		                    if(!choose_credits_bid.getText().toString().contains("Choose")){
								
								credits = Integer.parseInt(choose_credits_bid.getText().toString());
								
								dialog.cancel();
								
								if(Integer.parseInt(VoteVIP.credits.getText().toString().split(" ")[0])<credits){
									alert(v.getContext());
								}else{
									if(type!=0){
							    		new AsyncTaskGoWith().execute();
							    	}else{
									 new AsyncTaskVote().execute();	
							    	}
								}
									
							}
						}
		        		
		        	});
				}
				
			}
        	
        	
        	dialog.show();
        }else{
        	alert(this);
        }
	}
	
	private void alert(Context c){
		alert = new AlertDialog.Builder(c);
    	alert.setTitle(c.getString(R.string.alert_title));
    	alert.setMessage(c.getString(R.string.alert_buy_message));
    	alert.setPositiveButton(c.getString(R.string.ok_alert), new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				
			}
			
			
    	});
    	alert.show();
	}
	
	private class AsyncTaskVote extends AsyncTask<Void,Void,Boolean>{

		@Override
		protected Boolean doInBackground(Void... params) {
			// TODO Auto-generated method stub
			
			if(getIntent().getStringExtra("type").equals("votevip")){
				try {
					JSONObject json = (JSONObject)VoteVIP.array.get(getIntent().getIntExtra("position", -1));
					return AsyncOperations.putOnDB("http://95.110.196.250/weyspace/put_a_vote.php", new String[]{"nome","email","udid","pic","credits"}, 
							new String[]{json.getString("Name"),
							sp.getString("email", ""),json.getString("UDID"),
							json.getString("Picture"),String.valueOf(credits)});
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					return false;
				}
			}else{
				if(getIntent().getStringExtra("type").equals("statvip")){
				  if(ShowFacebookResults.groupPositionGlobal==0){
					try {
						return AsyncOperations.putOnDB("http://95.110.196.250/weyspace/put_a_vote.php", new String[]{"nome","email","udid","pic","credits"}, 
								new String[]{VoteVIP.pagesJSON.getJSONArray("data").getJSONObject(0).getString("name"),
								sp.getString("email", ""),VoteVIP.pagesJSON.getJSONArray("data").getJSONObject(0).getString("id")
										,VoteVIP.imagePage.getJSONObject("data").getString("url"),String.valueOf(credits)});
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						return false;
					}
				  }else{
					JSONObject image_url = (JSONObject)VoteVIP.arrayUsersImage.get(childPos);
					
					try {
						return AsyncOperations.putOnDB("http://95.110.196.250/weyspace/put_a_vote.php", new String[]{"nome","email","udid","pic","credits"}, 
								new String[]{VoteVIP.arrayUsersName.get(childPos).toString().split("!")[0],
								sp.getString("email", ""),VoteVIP.arrayUsersName.get(childPos).toString().split("!")[1]
										,image_url.getJSONObject("data").getString("url"),String.valueOf(credits)});
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						return false;
					}
				  }
				}else{
					if(getIntent().getStringExtra("type").equals("globvip")){
						try {
							JSONObject json = ((JSONObject)SatsVote.array.get(getIntent().getIntExtra("position", -1)));
							
							return AsyncOperations.putOnDB("http://95.110.196.250/weyspace/put_a_vote.php", new String[]{"nome","email","udid","pic","credits"}, 
									new String[]{json.getString("Name"),
									sp.getString("email", ""),json.getString("UDID")
											,json.getString("Picture"),String.valueOf(credits)});
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							return false;
						}
					}else{
						try {
							return AsyncOperations.putOnDB("http://95.110.196.250/weyspace/put_a_vote.php", new String[]{"nome","email","udid","pic","credits"}, 
									new String[]{Random.json.getJSONObject(getIntent().getIntExtra("position", -1)).getString("Name"),
									sp.getString("email", ""),Random.json.getJSONObject(getIntent().getIntExtra("position", -1)).getString("UDID")
											,Random.json.getJSONObject(getIntent().getIntExtra("position", -1)).getString("Picture"),String.valueOf(credits)});
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							return false;
						}	
					}
				}
			}
		}
		
		@Override
		protected void onPostExecute(Boolean success) {
			if(success){
				mainTab.credits = credits;
				mainTab.childPosition = childPos;
				mainTab.voted = true;
				
				new AsyncTaskVotedList().execute();
				
				alertSuccess();
				
			}
		}
		
	}
	
	private void alertSuccess(){
		
		Editor e = sp.edit();
    	e.putInt("credits", sp.getInt("credits", 0)-mainTab.credits);
    	e.commit();
    	new AsyncCreditsOp().execute(credits);
    	VoteVIP.requestBackup(this);
    	
		alert = new AlertDialog.Builder(this);
		alert.setTitle(getString(R.string.title_dialog_go_with));
		alert.setMessage(getString(R.string.message_dialog_go_with));
		alert.setNegativeButton(getString(R.string.negative_dialog_go_with),  new DialogInterface.OnClickListener(){

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				img.stop();
				img.cancelDisplayTask(my_pic);
				img.cancelDisplayTask(vote_pic);
				
				finish();
			}
			
		});
		alert.setPositiveButton(getString(R.string.positive_dialog_go_with), new DialogInterface.OnClickListener(){

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				VoteVIP.credits.setText(((int)Double.parseDouble(VoteVIP.credits.getText().
						toString().split(" ")[0])-mainTab.credits)+" "+getString(R.string.credits));
		    	
				buyStuff(1);
			}

			
			
		});
	    alert.show();
	}
	
	
	private class AsyncTaskGoWith extends AsyncTask<Void,Void,Boolean>{

		@Override
		protected Boolean doInBackground(Void... params) {
			// TODO Auto-generated method stub
			try {
				if(getIntent().getStringExtra("type").equals("votevip")){
					JSONObject json = (JSONObject)VoteVIP.array.get(getIntent().getIntExtra("position", -1));
					
					return AsyncOperations.putOnDB("http://95.110.196.250/weyspace/go_with.php", new String[]{"udid","email","credits"}, 
								new String[]{json.getString("UDID"),sp.getString("email", ""),String.valueOf(credits)});	
				}else{
					if(getIntent().getStringExtra("type").equals("statvip")){
						
						return AsyncOperations.putOnDB("http://95.110.196.250/weyspace/go_with.php", new String[]{"udid","email","credits"}, 
								new String[]{VoteVIP.arrayUsersName.get(childPos).toString().split("!")[1],sp.getString("email", ""),String.valueOf(credits)});
					}else{
						
						if(getIntent().getStringExtra("type").equals("globvip")){
							JSONObject json = ((JSONObject)SatsVote.array.get(getIntent().getIntExtra("position", -1)));
							return AsyncOperations.putOnDB("http://95.110.196.250/weyspace/go_with.php", new String[]{"udid","email","credits"}, 
									new String[]{json.getString("UDID")
									,sp.getString("email", ""),String.valueOf(credits)});	
						}else{
							return AsyncOperations.putOnDB("http://95.110.196.250/weyspace/go_with.php", new String[]{"udid","email","credits"}, 
									new String[]{Random.json.getJSONObject(getIntent().getIntExtra("position", -1)).getString("UDID")
									,sp.getString("email", ""),String.valueOf(credits)});	
						}

					}
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				return false;
			}
		}
		
		@Override
		protected void onPostExecute(Boolean success) {
			mainTab.credits = credits;
			mainTab.childPosition = childPos;
			mainTab.voted = true;
			
			img.stop();
			img.cancelDisplayTask(my_pic);
			img.cancelDisplayTask(vote_pic);
			
			finish();
		}
		
	}
	
	private class AsyncTaskGoWithJSON extends AsyncTask<Void,Void,JSONArray>{

		@Override
		protected JSONArray doInBackground(Void... params) {
			// TODO Auto-generated method stub
			
					
			try {
				return AsyncOperations.readMyJSON("http://95.110.196.250/weyspace/json.php", new String[]{"gwith"}, new String[]{"yes"});
			} catch (Exception e) {
				// TODO Auto-generated catch block
				return new JSONArray();
			}	
		}
		
		@Override
		protected void onPostExecute(JSONArray success) {
			for(int i=0;i<success.length();i++){
				try {
					if(getIntent().getStringExtra("type").equals("votevip")){
					  JSONObject json = (JSONObject)VoteVIP.array.get(getIntent().getIntExtra("position", -1));
					  if(success.getJSONObject(i).getString("User").equals(sp.getString("email", ""))  &&  
							success.getJSONObject(i).getString("Go_With").equals(json.getString("UDID"))){
						
						img.displayImage("http://graph.facebook.com/"+sp.getString("username_facebook", "")+"/picture?type=large", my_pic);
						
						break;
					  }
					}else{
						if(getIntent().getStringExtra("type").equals("statvip")){
							if(ShowFacebookResults.groupPositionGlobal==0){
								
								if(success.getJSONObject(i).getString("User").equals(sp.getString("email", ""))  &&  
										success.getJSONObject(i).getString("Go_With").equals(VoteVIP.pagesJSON.getJSONArray("data")
												.getJSONObject(0).getString("id"))){
									
									img.displayImage("http://graph.facebook.com/"+sp.getString("username_facebook", "")+"/picture?type=large", my_pic);
									
									break;
								}
							}else{
								if(success.getJSONObject(i).getString("User").equals(sp.getString("email", ""))  &&  
										success.getJSONObject(i).getString("Go_With").equals(VoteVIP.arrayUsersName.get(childPos).toString().split("!")[1])){
									
									img.displayImage("http://graph.facebook.com/"+sp.getString("username_facebook", "")+"/picture?type=large", my_pic);
									
									break;
								}
							}
						}else{
							if(getIntent().getStringExtra("type").equals("globvip")){
								JSONObject json = (JSONObject)SatsVote.array.get(getIntent().getIntExtra("position", -1));
								if(success.getJSONObject(i).getString("User").equals(sp.getString("email", ""))  &&  
										success.getJSONObject(i).getString("Go_With").equals(json.getString("UDID"))){
									
									img.displayImage("http://graph.facebook.com/"+sp.getString("username_facebook", "")+"/picture?type=large", my_pic);
									
									break;
								}
							}else{
								if(success.getJSONObject(i).getString("User").equals(sp.getString("email", ""))  &&  
										success.getJSONObject(i).getString("Go_With").equals(Random.json.getJSONObject(getIntent().getIntExtra("position", -1))
												.getString("UDID"))){
									
									img.displayImage("http://graph.facebook.com/"+sp.getString("username_facebook", "")+"/picture?type=large", my_pic);
									
									break;
								}
							}
							
						}
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
	}
	
	
	private String getImage() throws JSONException{
		if(this.getIntent().getStringExtra("type").equals("votevip")){
			JSONObject json = (JSONObject)VoteVIP.array.get(this.getIntent().getIntExtra("position", -1));
			return "http://graph.facebook.com/"+json.getString("UDID")+"/picture?type=large";	
		}else{
			if(this.getIntent().getStringExtra("type").equals("statvip")){
				if(ShowFacebookResults.groupPositionGlobal==0){
					return VoteVIP.imagePage.getJSONObject("data").getString("url");
				}else{
					return "http://graph.facebook.com/"+VoteVIP.arrayUsersName.get(childPos).toString().split("!")[1]+"/picture?type=large";
				}	
			}else{
				if(this.getIntent().getStringExtra("type").equals("globvip")){
					JSONObject json = (JSONObject)SatsVote.array.get(this.getIntent().getIntExtra("position", -1));
					System.out.println("foto glob"+json.getString("UDID"));
					return "http://graph.facebook.com/"+json.getString("UDID")+"/picture?type=large";
				}else{
					return "http://graph.facebook.com/"+Random.json.getJSONObject(this.getIntent().getIntExtra("position", -1)).getString("UDID")+"/picture?type=large";	
				}
			}
		}
	}
	
	private String getName() throws JSONException{
		if(this.getIntent().getStringExtra("type").equals("votevip")){
			return ((JSONObject)VoteVIP.array.get(this.getIntent().getIntExtra("position", -1))).getString("Name");
		}else{
			if(this.getIntent().getStringExtra("type").equals("statvip")){
			  if(ShowFacebookResults.groupPositionGlobal==0){
				return VoteVIP.pagesJSON.getJSONArray("data").getJSONObject(0).getString("name");	
			  }else{
				return VoteVIP.arrayUsersName.get(ShowFacebookResults.childPositionGlobal).toString().split("!")[0];
			  }
			}else{
				if(this.getIntent().getStringExtra("type").equals("globvip")){
					return ((JSONObject)SatsVote.array.get(this.getIntent().getIntExtra("position", -1))).getString("Name");
				}else{
					return Random.json.getJSONObject(this.getIntent().getIntExtra("position", -1)).getString("Name");	
				}
			}
		}
	}
	
	private class AsyncCreditsOp extends AsyncTask<Integer,Void,Void>{

		@Override
		protected Void doInBackground(Integer... params) {
			// TODO Auto-generated method stub
			System.out.println("prova "+VoteVIP.credits.getText().toString().split(" ")[0]);
			AsyncOperations.putOnDB("http://95.110.196.250/weyspace/credits_used.php", 
					new String[]{"email","bid","credits","dec"}, new String[]{sp.getString("email", ""),
					String.valueOf(params[0]),VoteVIP.credits.getText().toString().split(" ")[0],"yes"});
			return null;
		}
		
	}
	
	public boolean onOptionsItemSelected(com.actionbarsherlock.view.MenuItem item) {
		img.stop();
		img.cancelDisplayTask(my_pic);
		img.cancelDisplayTask(vote_pic);
		
		finish();
	    return true;
	}

}
