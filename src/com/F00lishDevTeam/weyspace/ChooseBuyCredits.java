package com.F00lishDevTeam.weyspace;

import org.holoeverywhere.app.Activity;

import com.F00lishDevTeam.weyspace.in_app.util.IabHelper;
import com.F00lishDevTeam.weyspace.in_app.util.IabResult;
import com.F00lishDevTeam.weyspace.in_app.util.Purchase;
import com.facebook.Session;
import com.lib.asyncHTTP.AsyncOperations;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;

public class ChooseBuyCredits extends Activity{
	private ImageView credits4,credits40,credits80,credits200;
	private Activity act;
	private String firstLine64,secondLine64,thirdLine64,fourthLine64,encodedKey;
	private IabHelper mHelper;
	private SharedPreferences sp;
	
	public void onCreate(Bundle b){
		super.onCreate(b);
		
		setContentView(R.layout.choose_buy_credits);
		
		this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		
		credits4 = (ImageView)findViewById(R.id.credits_4);
		credits40 = (ImageView)findViewById(R.id.credits_40);
		credits80 = (ImageView)findViewById(R.id.credits_80);
		credits200 = (ImageView)findViewById(R.id.credits_200);
		
		firstLine64 = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEArm5/fj+1liG/sgi8fT9bBD/2XQKJKppXJlhmhDMyYtzgsJ+w2ImsgsfMZATdZ05CktshB3Kbx";
		secondLine64 = "4o8vd+uCRFUNRWB9HZr9UIsGbEAQa60vEuZ6n+sEHDUqfhFZDWtdJyK6mkbaNhvIxl75Wv3etcyt6FAYD13Y33WGdwfNw4qe6sj323X9WY5kbYj9n+kF4";
		thirdLine64 = "gthi8V6XBBpC1zfyuUsYdCySRaOiaumKohOn40rQG87fhF3IKfey2DHrbKumjpD61iUl8lD2uZJLJh76BhRS3tGqTMUuozl9koE4Q57zf1gMa6Zs1rtg0";
		fourthLine64 = "uikhjTooTf1FokTA/BvLz7lnv5xNjZct80wIDAQAB";
		
		sp = this.getSharedPreferences("login", MODE_PRIVATE);
		  
		encodedKey = firstLine64+secondLine64+thirdLine64+fourthLine64;
		
		act = this;
		
		mHelper = new IabHelper(this,encodedKey);
		
		
		mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
	          public void onIabSetupFinished(IabResult result) {

	              if (!result.isSuccess()) {
	                  // Oh noes, there was a problem.
	                  //complain("Problem setting up in-app billing: " + result);
	                  return;
	              }

	              // Hooray, IAB is fully set up. Now, let's get an inventory of stuff we own.
	              
	              //mHelper.queryInventoryAsync(mGotInventoryListener);
	          }
	    });
		
		
		credits4.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mHelper.launchPurchaseFlow(act, "crediti4", 1001, 
		                mPurchaseFinishedListener, "");
			}
			
		});
		
		
		credits40.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mHelper.launchPurchaseFlow(act, "crediti41", 1001, 
		                mPurchaseFinishedListener, "");
			}
			
		});
		
		
		credits80.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mHelper.launchPurchaseFlow(act, "crediti81", 1001, 
		                mPurchaseFinishedListener, "");
			}
			
		});
		
		credits200.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mHelper.launchPurchaseFlow(act, "crediti201", 1001, 
		                mPurchaseFinishedListener, "");
			}
			
		});
		
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy(); 
		if (mHelper != null){ 
			mHelper.dispose();
            mHelper = null;
		}
        
        
	}
	
	IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
        public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
            //Log.d(TAG, "Purchase finished: " + result + ", purchase: " + purchase);
            if (result.isFailure()) {
                return;
            }

            //Log.d(TAG, "Purchase successful.");

            if (purchase.getSku().equals("crediti4") || purchase.getSku().equals("crediti41") || purchase.getSku().equals("crediti81") 
            		|| purchase.getSku().equals("crediti201")) {
                // bought 1/4 tank of gas. So consume it.
                //Log.d(TAG, "Purchase is gas. Starting gas consumption.");
                mHelper.consumeAsync(purchase, mConsumeFinishedListener);
            }
            /*else if (purchase.getSku().equals(SKU_PREMIUM)) {
                // bought the premium upgrade!
                Log.d(TAG, "Purchase is premium upgrade. Congratulating user.");
                alert("Thank you for upgrading to premium!");
                mIsPremium = true;
                updateUi();
                setWaitScreen(false);
            }
            else if (purchase.getSku().equals(SKU_INFINITE_GAS)) {
                // bought the infinite gas subscription
                Log.d(TAG, "Infinite gas subscription purchased.");
                alert("Thank you for subscribing to infinite gas!");
                mSubscribedToInfiniteGas = true;
                mTank = TANK_MAX;
                updateUi();
                setWaitScreen(false);
            }*/
        }
    };
    
    
    IabHelper.OnConsumeFinishedListener mConsumeFinishedListener = new IabHelper.OnConsumeFinishedListener() {
        public void onConsumeFinished(Purchase purchase, IabResult result) {
            //Log.d(TAG, "Consumption finished. Purchase: " + purchase + ", result: " + result);

            // We know this is the "gas" sku because it's the only one we consume,
            // so we don't check which sku was consumed. If you have more than one
            // sku, you probably should check...
            if (result.isSuccess()) {
                // successfully consumed, so we apply the effects of the item in our
                // game world's logic, which in our case means filling the gas tank a bit
                /*Log.d(TAG, "Consumption successful. Provisioning.");
                mTank = mTank == TANK_MAX ? TANK_MAX : mTank + 1;
                saveData();
                alert("You filled 1/4 tank. Your tank is now " + String.valueOf(mTank) + "/4 full!");*/
            	
            	//fare tutte le operazioni necessarie per caricare i crediti
            	
            	Editor e = sp.edit();
            	if(purchase.getSku().equals("crediti4")){
            		e.putInt("credits", sp.getInt("credits", 0)+4);
            		
            		new AsyncBuy().execute(4);
            	}

            	if(purchase.getSku().equals("crediti41")){
            		e.putInt("credits", sp.getInt("credits", 0)+40);	
            		
            		new AsyncBuy().execute(40);
            	}
            	
            	if(purchase.getSku().equals("crediti81")){
            		e.putInt("credits", sp.getInt("credits", 0)+80);
            		
            		new AsyncBuy().execute(80);
            	}
            	
            	if(purchase.getSku().equals("crediti201")){
            		e.putInt("credits", sp.getInt("credits", 0)+200);	
            		
            		new AsyncBuy().execute(200);
            	}
            	
            	e.commit();
            	VoteVIP.credits.setText(sp.getInt("credits", 0)+" "+act.getString(R.string.credits));
            	
            	VoteVIP.requestBackup(act.getApplicationContext());
            	
            }
            else {
                //complain("Error while consuming: " + result);
            }
            /*updateUi();
            setWaitScreen(false);
            Log.d(TAG, "End consumption flow.");*/
        }
    };
    
    @Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (!mHelper.handleActivityResult(requestCode, resultCode, data)) {
	      super.onActivityResult(requestCode, resultCode, data);
	      Session.getActiveSession().onActivityResult(this, requestCode, resultCode, data);
		}
	}
    
    @Override
	public boolean onOptionsItemSelected(com.actionbarsherlock.view.MenuItem item) {
		//this.startActivityForResult(new Intent(getApplicationContext(),mainTab.class),1);
		finish();
	    return true;
	}
    
    private class AsyncBuy extends AsyncTask<Integer,Void,Void>{

		@Override
		protected Void doInBackground(Integer... params) {
			// TODO Auto-generated method stub
			
			AsyncOperations.putOnDB("http://95.110.196.250/weyspace/credits_used.php", 
					new String[]{"email","bid","credits","inc"}, new String[]{sp.getString("email", ""),
					String.valueOf(params[0]),VoteVIP.credits.getText().toString().split(" ")[0],"yes"});
			
			AsyncOperations.putOnDB("http://95.110.196.250/weyspace/credits_bought.php", new String[]{"credits","email"}, 
					new String[]{String.valueOf(params[0]),sp.getString("email", "")});
			
			return null;
		}
    	
    }
}
