package com.F00lishDevTeam.weyspace;

import java.util.ArrayList;

import org.holoeverywhere.app.Activity;
import org.holoeverywhere.widget.Button;
import org.holoeverywhere.widget.LinearLayout;
import org.holoeverywhere.widget.ListView;
import org.json.JSONArray;
import org.json.JSONObject;

import com.lib.asyncHTTP.AsyncOperations;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.RelativeLayout;
import android.widget.AdapterView.OnItemClickListener;

public class Random extends Activity{
	private ListView list;
	private Button country;
	private Handler mHandler;
	private Thread t;
	private LinearLayout.LayoutParams lp;
	private LinearLayout linear;
	
	public static JSONArray json;
	
	public void onCreate(Bundle b){
		super.onCreate(b);
		setContentView(R.layout.statsvote);
		
		country = (Button)findViewById(R.id.countries_list);
		country.setVisibility(country.INVISIBLE);
		mHandler = new Handler();
		
		list = (ListView)findViewById(R.id.nations_list);
		
		lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
		
		linear = (LinearLayout)findViewById(R.id.linear_stat);
		
		linear.removeViewInLayout(country);
		
		list.setLayoutParams(lp);
		
		t = new Thread(new Runnable() {
	        @Override
	        public void run() {
	            // TODO Auto-generated method stub
	            while (true) {
	                try {
	                    mHandler.post(new Runnable() {

	                        @Override
	                        public void run() {
	                            // TODO Auto-generated method stub
	                            // Write your code here to update the UI.
	                        	 new AsyncRandom().execute();
	                        }
	                    });
	                    Thread.sleep(10000);
	                } catch (Exception e) {
	                    // TODO: handle exception
	                }
	            }
	        }
	    });
		
		
		t.start();
	
	}
	
	
	
    private class AsyncRandom extends AsyncTask<Void,Void,JSONArray>{
		
		protected JSONArray  doInBackground(Void... params) {
			// TODO Auto-generated method stub
			
			try {
				return AsyncOperations.readMyJSON("http://95.110.196.250/weyspace/json.php", new String[]{"random"}, new String[]{"yes"});
			} catch (Exception e) {
				// TODO Auto-generated catch block
				return new JSONArray();
			}
		}
		
		@Override
		protected void onPostExecute(JSONArray json) {
    		
			//Random.country.setVisibility(View.VISIBLE);
			Random.json = json;
			System.out.println("prova "+VoteVIP.array.toString());
			
			Parcelable state = list.onSaveInstanceState();
			
			list.setAdapter(new ListVotedRandom(getApplicationContext(),R.layout.list_voted,R.id.name_voted, VoteVIP.array,json));
			
			list.onRestoreInstanceState(state);
			
			list.setOnItemClickListener(new OnItemClickListener(){

				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1,
						int arg2, long arg3) {
					// TODO Auto-generated method stub
					startActivity(new Intent(getApplicationContext(),VotePRSG.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK).putExtra("position", arg2)
							.putExtra("type", "randomvip"));
				}
				
			});
	    }
	}
    
    public boolean onMenuItemSelected (int featureId, com.actionbarsherlock.view.MenuItem item){
		
		switch(item.getItemId()){
		  case R.id.menu_forgot_password:
			  startActivity(new Intent(getApplicationContext(),Recovery.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
			break;
			
		  case R.id.send_email:
			  Intent intent = new Intent (Intent.ACTION_VIEW , Uri.parse("mailto:" + "help@weyspace.com"));
			  intent.putExtra(Intent.EXTRA_SUBJECT, "Help!");
			  startActivity(intent); 
			  
			  break;
		}
		
		return true;
	}
	
	public boolean onCreateOptionsMenu(com.actionbarsherlock.view.Menu menu) {
		super.onCreateOptionsMenu(menu);
		this.getSupportMenuInflater().inflate(R.menu.recovery, menu);
		return true;
	}
	

}
