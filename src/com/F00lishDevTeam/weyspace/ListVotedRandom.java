package com.F00lishDevTeam.weyspace;

import java.io.File;
import java.util.ArrayList;

import org.holoeverywhere.app.AlertDialog;
import org.holoeverywhere.app.Dialog;
import org.holoeverywhere.preference.SharedPreferences;
import org.holoeverywhere.widget.Button;
import org.holoeverywhere.widget.EditText;
import org.holoeverywhere.widget.ProgressBar;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.lib.asyncHTTP.AsyncOperations;
import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.DisplayImageOptions.Builder;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import android.content.Context;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ListVotedRandom extends ArrayAdapter<String>{
	Context c;
	int layout;
	static JSONArray json;
	ImageView image;
	TextView name;
	ProgressBar percentage;
	ImageLoaderConfiguration config;
	ImageLoader img;
	double count = 0;
	double percent_init=0,percent_do=0;
	double total_votes;
	
	private static Dialog dialog;
	private static EditText choose_credits_bid;
	private static Button ok_credits;
	private static int credits,childPos;
	private static AlertDialog.Builder alert;
	private static android.content.SharedPreferences sp;
	private boolean random = false;
	private DisplayImageOptions d;
	
	public ListVotedRandom(Context context, int resource, int textViewResourceId,
			ArrayList objects, JSONArray response) {
		super(context, resource, textViewResourceId, objects);
		// TODO Auto-generated constructor stub
		d = new DisplayImageOptions.Builder().cacheInMemory().build();
		c = context; layout = resource; json = response; config = new ImageLoaderConfiguration.Builder(c).
				threadPoolSize(ImageLoaderConfiguration.Builder.DEFAULT_THREAD_POOL_SIZE).defaultDisplayImageOptions(d).build(); img = ImageLoader.getInstance();
		img.init(config);
		count = objects.size();
		
		sp = context.getSharedPreferences("login", context.MODE_PRIVATE);
		
		try {
			total_votes = getAllVotes();
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private double getAllVotes() throws NumberFormatException, JSONException{
		
		double tot = 0;
		
		for(int i=0;i<count;i++){
			tot += Double.parseDouble(json.getJSONObject(i).getString("Votes"));	
		}
		
		return tot;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) c
		        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		    View rowView = inflater.inflate(layout, parent, false);
		    
		    
		    
		    image = (ImageView)rowView.findViewById(R.id.picture);
		    name = (TextView)rowView.findViewById(R.id.name_voted);
		    percentage = (ProgressBar)rowView.findViewById(R.id.vote_voted);
		    
		    percentage.setVisibility(percentage.INVISIBLE);
		    
		    try {
				name.setText(json.getJSONObject(position).getString("Name"));
				
				
				percentage.setProgress((int) (Double.parseDouble(json.getJSONObject(position).getString("Votes"))*100/total_votes));	
				
				
				img.displayImage(json.getJSONObject(position).getString("Picture"), image);
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		    
		    return rowView;
	}
	
	

}
